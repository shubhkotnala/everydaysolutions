# Everyday Solutions

**This project is still under development, stay updated with the daily builds**
A project aiming to help people get their work done easily by finding the workers near them easily.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to setup the project on your system?

**This project uses Gradle build system**

* Gradle
* Andoid Studio 3
* Androis SDK (API 26 & 27)
* Java8 (For Lambda Functions)
* Google Play Services (for Device)

```
This project is still in JAVA so Android Studio version < 3 can be used but Kotlin maybe used for future so v3 is recommended.
```

### Installing

A step by step series of examples that tell you have to get a development environment running on your local machine

* Fork, Clone or Download the repository.
* Open Android Studio and import the project file
* Login to your Google account and create a new Firebase Project
* Setup Firebase in the project

That's it. You can now run the project on your machine. Enjoy!

```
To run the project on your device directly you need to setup ADB tools if running on Windows.
```

## Deployment

**Not a working project yet**

To use this on a live system you need to first setup Firebase.
For this visit Firebase Console and enable E-Mail ID and Phone Number Verification.
You'll also need to add the SHA5 key of your signature so that the app may be authenticated.

## Built With

* [Android Studio](https://developer.android.com/studio/) - The official IDE for Google's Android Development
* [Firebase](https://firebase.google.com/) - Google's BaaS

## Contributing

Please contact the developer for knowing the code of conduct, and the process for submitting the pull requests to us.

## Authors

* **Shubhankar Kotnala** - *Initial work* - [ShubhKotnala](https://bitbucket.org/shubhkotnala)

#### Note

This project is not under development as of 20 May, 2018.