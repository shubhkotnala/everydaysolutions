package com.dit.ananya.atservices;


        import android.content.Intent;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.Button;
        import android.widget.RelativeLayout;
        import android.widget.Toast;


public class StartActivity extends AppCompatActivity {

    private Button mRegBtn;
    private Button mLoginBtn;
    private RelativeLayout mRelativeLayout;
    private Button mPhoneAuthBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        getSupportActionBar().setElevation(0);

       // mRegBtn=(Button) findViewById(R.id.start_reg_button);
        mLoginBtn=(Button) findViewById(R.id.start_login_btn);
       // mPhoneAuthBtn = (Button) findViewById(R.id.btnPhone);


       /* mRegBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reg_intent= new Intent(StartActivity.this, Register.class);
                startActivity(reg_intent);
            }
        });
*/

        mLoginBtn.setOnClickListener( view -> {

            startActivity(new Intent(StartActivity.this,PhoneAuthActivity.class));

        });

        /*mPhoneAuthBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StartActivity.this,PhoneAuthActivity.class));
            }
        });*/


    }
}