package com.dit.ananya.atservices;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by shubhkotnala on 10/2/18.
 */

public class SplashActivity extends AppCompatActivity{

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


     /*   new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(SplashActivity.this,MainActivity.class));
                SplashActivity.this.finish();

            }
        },2500);
 */

        new Handler().postDelayed(() -> {

            startActivity(new Intent(SplashActivity.this,MainActivity.class));
            SplashActivity.this.finish();

        },2500);

    }
}
