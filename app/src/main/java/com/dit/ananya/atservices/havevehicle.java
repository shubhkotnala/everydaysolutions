package com.dit.ananya.atservices;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;

public class havevehicle extends AppCompatActivity {
    private final int REQUEST_CODE_PLACEPICKER = 1;
    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private DatabaseReference mDatabase,mLocationDatabase;
    private EditText spf;
    private EditText spt;
    private Button saveBtn;
    String place ="";
    String address="";
    String addre;
    private EditText vn,cn,tc,dateOfTravel,timeOfTravel;
    String name,email,image,phone,from,to,verificationImage;
    String verify= null;
    private int SELECTED = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_havevehicle);
        saveBtn = (Button) findViewById(R.id.saveBtn);
        vn = (EditText) findViewById(R.id.vn) ;
        tc = (EditText) findViewById(R.id.tc);
        cn = (EditText) findViewById(R.id.cn) ;
        dateOfTravel = (EditText) findViewById(R.id.dateOfTravel);
        timeOfTravel = (EditText) findViewById(R.id.timeOfTravel);

        getSupportActionBar().setElevation(0);

        spf = (EditText) findViewById(R.id.spf);
        spt = (EditText) findViewById(R.id.spt);
        spf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            //    startPlacePickerActivity();

                SELECTED = 3;

                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                    .build(havevehicle.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

                 //   spf.setText("" + from);

                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
               // from= place;

            }
        });
        spt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SELECTED = 2;

                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                    .build(havevehicle.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);



                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
           // to = place;
            }

        });

        FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
       final String uid = current_user.getUid();
        mDatabase=FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
        mLocationDatabase=FirebaseDatabase.getInstance().getReference().child("Location").child(uid);


        mDatabase.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                name =dataSnapshot.child("name").getValue().toString();
                email =dataSnapshot.child("email").getValue().toString();
                image = dataSnapshot.child("image").getValue().toString();
                phone = dataSnapshot.child("phone").getValue().toString();
                verificationImage = dataSnapshot.child("verificationImageUrl").getValue().toString();
                verify = dataSnapshot.child("verify").getValue().toString();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        dateOfTravel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker;
                mDatePicker = new DatePickerDialog(havevehicle.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                    /*      Your code   to get date and time    */
                        selectedmonth = selectedmonth + 1;
                        dateOfTravel.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select Date");
                mDatePicker.show();
            }
        });

        timeOfTravel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(havevehicle.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        timeOfTravel.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  spf.setOnItemSelectedListener(new CustomOnItemSelectedListener());
                final String selected = spf.getText().toString();
                //Toast.makeText(havevehicle.this, selected, Toast.LENGTH_LONG).show();
                //  Toast.makeText(havevehicle.this, addre, Toast.LENGTH_SHORT).show();

                //  spt.setOnItemSelectedListener(new CustomOnItemSelectedListener());
                final String selectedTo = spt.getText().toString();
                //Toast.makeText(havevehicle.this, selected, Toast.LENGTH_LONG).show();

                String vno = cn.getText().toString().trim();
                String vName = vn.getText().toString().trim();
                String tCap = tc.getText().toString().trim();


                if (TextUtils.isEmpty(vno) || TextUtils.isEmpty(vName) || TextUtils.isEmpty(tCap)) {

                    if(TextUtils.isEmpty(vno)){
                        cn.setError("Enter Vehicle number");
                    }
                    if(TextUtils.isEmpty(vName)){
                        vn.setError("Enter Vehicle Name");
                    }
                    if(TextUtils.isEmpty(tCap)){
                        tc.setError("Enter Vehicle capacity");
                    } if (timeOfTravel == null){
                        timeOfTravel.setError("Enter time");
                    }if (dateOfTravel == null){
                        dateOfTravel.setError("Enter Date");
                    }

                } else if(selected.equals("SELECT LOCATION")||selectedTo.equals("SELECT LOCATION")){
                   // Toast.makeText(havevehicle.this,"Select Location",Toast.LENGTH_SHORT).show();
                } else{

                    String time = timeOfTravel.getText().toString();
                    final String date = dateOfTravel.getText().toString();

                    HashMap<String,String> userMap= new HashMap<String, String>();

                    userMap.put("name",name);
                    userMap.put("email",email);
                    userMap.put("image",image);
                    userMap.put("phone",phone);
                    userMap.put("from",from);
                    userMap.put("to",to);
                    userMap.put("vehicle",vName);
                    userMap.put("number",vno);
                    userMap.put("capacity",tCap);
                    userMap.put("time",time);
                    userMap.put("date",date);
                    userMap.put("verify",verify);
                    userMap.put("verificationImage",verificationImage);
                    userMap.put("address",addre);
                    userMap.put("uid",uid);
                    userMap.put("search",from+to+date+"hello");

                    mLocationDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if(task.isSuccessful()){


                                Intent intent = new Intent(havevehicle.this,Searchv.class);
                                intent.putExtra("fromv",selected);
                                intent.putExtra("tov",selectedTo);
                                intent.putExtra("date",date);


                                startActivity(intent);
                                //   Toast.makeText(havevehicle.this,"Successful", Toast.LENGTH_LONG).show();
                            }
                        }

                    });
                }

            }
        });
    }

    private void startPlacePickerActivity() {


        PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
        // this would only work if you have your Google Places API working

        try {
            Intent intent = intentBuilder.build(this);
            startActivityForResult(intent, REQUEST_CODE_PLACEPICKER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void displaySelectedPlaceFromPlacePicker(Intent data) {
        place = "";
        Place placeSelected = PlacePicker.getPlace(data, this);
        place = placeSelected.getName().toString();
        address=placeSelected.getAddress().toString();
        if(SELECTED == 2){

            spt.setText(place);
            to=place;

        } else if(SELECTED == 3){
            spf.setText(place);
            from=place;
            addre=address;

        }

    }
    protected  void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PLACEPICKER && resultCode == RESULT_OK) {

            displaySelectedPlaceFromPlacePicker(data);

        }

        if(requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE && resultCode == RESULT_OK){

            displaySelectedPlaceFromPlacePicker(data);

        }
    }



}

