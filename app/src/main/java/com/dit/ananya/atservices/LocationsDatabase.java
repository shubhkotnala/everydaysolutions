package com.dit.ananya.atservices;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class LocationsDatabase extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private Button mUploadBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations_database);
        getSupportActionBar().setElevation(0);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Places").child("States");

        mUploadBtn = (Button) findViewById(R.id.btnSaveData);

        mUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile();
            }
        });

    }

    public void uploadFile() {

        InputStream inputStream = this.getResources().openRawResource(R.raw.states);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

        List<String> wordList = new ArrayList<>();

        try{

            String in="";
            while((in = br.readLine()) != null){

                String words[] = in.split("~");

                for(int i=0 ; i<words.length ; i++){

                    wordList.add(words[i]);

                }

            }

            mDatabase.setValue(wordList).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    if(task.isSuccessful()){
                        Toast.makeText(LocationsDatabase.this,"Upload Complete",Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(LocationsDatabase.this,"Upload Unsuccessful",Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }catch (FileNotFoundException e){
            Log.d("File","Not found");
            Toast.makeText(LocationsDatabase.this,"File Not Found",Toast.LENGTH_SHORT).show();
        }catch (IOException e){
            Log.d("File","I/O Error");
            Toast.makeText(LocationsDatabase.this,"I./O Error",Toast.LENGTH_SHORT).show();
        }

    }

}
