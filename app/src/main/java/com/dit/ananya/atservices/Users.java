package com.dit.ananya.atservices;


public class Users {

    public String from;
    public String to;
    public String vehicle;
    public String number;
    public String capacity;
    public String name;
    public String email;
    public String image;
    public String date;
    public String time;
    public String phone;
    public String verify;
    public String uid;
    public String address;
    public Users(){

    }

    public Users(String from,String uid, String to, String vehicle, String number,String address, String capacity,String name,String email,String image,String date,String time,String phone,String verify) {
        this.from = from;
        this.to = to;
        this.vehicle = vehicle;
        this.number = number;
        this.capacity = capacity;
        this.name= name;
        this.email=email;
        this.image = image;
        this.date= date;
        this.time= time;
        this.phone= phone;
        this.verify=verify;
        this.uid=uid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email =email ;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date =date ;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time =time ;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone =phone ;
    }
    public String getVerify() {
        return verify;
    }

    public void setVerify(String verify) {
        this.verify =verify ;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}



