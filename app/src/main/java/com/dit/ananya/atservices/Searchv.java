package com.dit.ananya.atservices;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class Searchv extends AppCompatActivity{

    private RecyclerView mResultList;

    private DatabaseReference mUserDatabase,mDatabase;
    String selected;
    String selectedTo;
    String selectedDate;
    String uid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getSupportActionBar().setElevation(0);
        mUserDatabase = FirebaseDatabase.getInstance().getReference("Location");
        mResultList = (RecyclerView) findViewById(R.id.result_list);
        mResultList.setHasFixedSize(true);
        mResultList.setLayoutManager(new LinearLayoutManager(this));
        Bundle extras = getIntent().getExtras();



        String selected = extras.getString("fromv");
        String selectedDate = extras.getString("date");
        String selectedTo = extras.getString("tov");
        String hello = "hello";

            firebaseUserSearch(selected, selectedTo,selectedDate);


    }



    private void firebaseUserSearch(final String selected, final String selectedTo,String selectedDate) {
        this.selected = selected;
        this.selectedTo = selectedTo;
        this.selectedDate = selectedDate;

        Query firebaseSearchQuery = mUserDatabase.orderByChild("search").startAt(selected + selectedTo +selectedDate + "hello").endAt(selected + selectedTo + selectedDate+"hello" + "\uf8ff");

        //Toast.makeText(this, selected+selectedTo+selectedDate+"hello", Toast.LENGTH_SHORT).show();
        // Query firebaseSearchQuery = mUserDatabase.orderByChild("search");//.startAt("ddn").endAt("ddn" + "\uf8ff");

        final FirebaseRecyclerAdapter<Users, UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Users, UsersViewHolder>(

                Users.class,
                R.layout.search_layout,
                UsersViewHolder.class,
                firebaseSearchQuery


        ) {

            @Override
            protected void populateViewHolder(UsersViewHolder viewHolder, Users model, int position) {
                final String name = model.getName();
                final String vehicle = model.getVehicle();
                final String image = model.getImage();
                final String phone = model.getPhone();
                final String email = model.getEmail();
                final String date = model.getDate();
                final String time = model.getTime();
                final String verify = model.getVerify();
                final String uid =model.getUid();
                final String address=model.getAddress();
                final String capacity=model.getCapacity();

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(Searchv.this,name,Toast.LENGTH_SHORT).show();
                        Intent searchIntent = new Intent(Searchv.this, UserSearchActivity.class);
                        searchIntent.putExtra("name", name);
                        searchIntent.putExtra("vehicle", vehicle);
                        searchIntent.putExtra("image", image);
                        searchIntent.putExtra("phone", phone);
                        searchIntent.putExtra("date", date);
                        searchIntent.putExtra("time", time);
                        searchIntent.putExtra("verify", verify);
                        searchIntent.putExtra("uid", uid);
                        searchIntent.putExtra("address",address);
                        searchIntent.putExtra("capacity",capacity);


                        startActivity(searchIntent);
                    }
                });


                viewHolder.setDetails(getApplicationContext(), model.getName(), model.getVehicle(), model.getImage());

            }
        };


        mResultList.setAdapter(firebaseRecyclerAdapter);


    }


    // View Holder Class

    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public UsersViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }


        public void setDetails(Context applicationContext, String user_name, String user_vname,String image){

            TextView username = (TextView) mView.findViewById(R.id.username);
            TextView uservname = (TextView) mView.findViewById(R.id.uservname);
            //TextView usercname =(TextView) mView.findViewById(R.id.usercname);
           // TextView usercapacity = (TextView) mView.findViewById(R.id.usercapacity);
            username.setText(user_name);
            uservname.setText(user_vname);
            CircleImageView imageView = (CircleImageView) mView.findViewById(R.id.imageView3);
            Picasso.with(applicationContext).load(image).placeholder(R.drawable.defaultpic).into(imageView);
            //usercname.setText(user_cname);
            //usercapacity.setText(user_capacity);


        }

    }

}
