package com.dit.ananya.atservices;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.CircularProgressButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;


public class Login extends AppCompatActivity {

    private Toolbar mToolbar;
    private TextInputLayout mLoginEmail;
    private TextInputLayout mLoginPassword;
    private CircularProgressButton mLogin_btn;
    private CircularProgressButton mReg_btn;
    private ProgressDialog mLoginProgress;
    private FirebaseAuth mAuth;
    private DatabaseReference mUserDatabase;
    private TextView forgetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().setElevation(0);
        mAuth= FirebaseAuth.getInstance();

        mLoginProgress = new ProgressDialog(this);
        mUserDatabase= FirebaseDatabase.getInstance().getReference().child("Users");

        mLoginEmail=(TextInputLayout) findViewById(R.id.login_email);
        mLoginPassword=(TextInputLayout)findViewById(R.id.login_password);
        mLogin_btn=(CircularProgressButton) findViewById(R.id.login_btn);
        mReg_btn=(CircularProgressButton) findViewById(R.id.reg_btn);
        forgetPassword = (TextView) findViewById(R.id.txtForget);

        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Login.this,ForgetPassword.class));

            }
        });
        mReg_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Login.this,Register.class));

            }
        });

        mLogin_btn.setIndeterminateProgressMode(true);
        mLogin_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLogin_btn.setProgress(50);
                String email= mLoginEmail.getEditText().getText().toString();
                String password= mLoginPassword.getEditText().getText().toString();

                if(!TextUtils.isEmpty(email)|| !TextUtils.isEmpty(password)){

                    mLoginProgress.setTitle("Logging In");
                    mLoginProgress.setMessage("Please wait while we check your credentials.");
                    mLoginProgress.setCanceledOnTouchOutside(false);
                   // mLoginProgress.show();

                    loginUser(email,password);
                }
            }
        });
    }

    private void loginUser(String email, String password) {

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                  //  mLoginProgress.dismiss();
                    mLogin_btn.setProgress(100);
                    String current_user_id=mAuth.getCurrentUser().getUid();
                    String deviceToken= FirebaseInstanceId.getInstance().getToken();

                    if(FirebaseAuth.getInstance().getCurrentUser().isEmailVerified()){
                        mUserDatabase.child(current_user_id).child("device_token").setValue(deviceToken)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Intent mainIntent= new Intent(Login.this, MainActivity.class );
                                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(mainIntent);
                                        finish();
                                    }
                                });
                    }else {

                        Toast.makeText(Login.this,"Verify your email and try again",Toast.LENGTH_SHORT).show();

                    }


                }
                else{
                    mLogin_btn.setProgress(-1);
                  //  mLoginProgress.hide();
                    Toast.makeText(Login.this,"Cannot sign in. Please try again later.",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}