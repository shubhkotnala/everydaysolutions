package com.dit.ananya.atservices;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class PhoneAuthActivity extends AppCompatActivity implements
        View.OnClickListener {

    EditText mPhoneNumberField, mVerificationField, mDisplayName;
    Button mStartButton, mVerifyButton, mResendButton;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    String mVerificationId;
    private ProgressDialog mRegProgress;

    private static final String TAG = "PhoneAuthActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_auth);

        mPhoneNumberField = (EditText) findViewById(R.id.field_phone_number);
        mVerificationField = (EditText) findViewById(R.id.field_verification_code);
        mDisplayName = (EditText) findViewById(R.id.field_display_name);

        mStartButton = (Button) findViewById(R.id.button_start_verification);
        mVerifyButton = (Button) findViewById(R.id.button_verify_phone);
        mResendButton = (Button) findViewById(R.id.button_resend);


        mVerifyButton.setVisibility(View.GONE);
        mResendButton.setVisibility(View.GONE);

        mRegProgress = new ProgressDialog(this);

        mStartButton.setOnClickListener(this);
        mVerifyButton.setOnClickListener(this);
        mResendButton.setOnClickListener(this);


        mAuth = FirebaseAuth.getInstance();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.d(TAG, "onVerificationCompleted:" + credential);
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    mPhoneNumberField.setError("Invalid phone number.");
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded." +
                                    "Please try after some time or try using another number",
                            Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                mResendButton.setEnabled(true);
                mVerifyButton.setEnabled(true);
                mResendButton.setVisibility(View.VISIBLE);
                mVerifyButton.setVisibility(View.VISIBLE);
                Log.d(TAG, "onCodeSent:" + verificationId);
                mVerificationId = verificationId;
                mResendToken = token;
                mRegProgress.dismiss();

            }
        };
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();

                            //Extras
                            FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
                            String uid= current_user.getUid();
                         //   String display_name = mDisplayName.getText().toString();
                            // String current_user_id=mAuth.getCurrentUser().getUid();
                            String deviceToken= FirebaseInstanceId.getInstance().getToken();
                            mDatabase= FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

                            String device_token= FirebaseInstanceId.getInstance().getToken();



                            mDatabase= FirebaseDatabase.getInstance().getReference().child("Users");
                            final String userid = uid;
                            mDatabase.addValueEventListener(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                                    if (!dataSnapshot.hasChild(userid)) {

                                                                     //   showNameDialog(userid);
                                                                        Intent shownameIntent = new Intent(PhoneAuthActivity.this,ShowNameDialog.class);
                                                                        shownameIntent.putExtra("uid",userid);
                                                                        shownameIntent.putExtra("phone",mPhoneNumberField.getText().toString());
                                                                        startActivity(shownameIntent);
                                                                        finish();
                                                                    }
                                                                    else{
                                                                        Intent mainIntent= new Intent(PhoneAuthActivity.this,MainActivity.class);
                                                                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                        startActivity(mainIntent);
                                                                        finish();
                                                                    }
                                                                }

                                                                @Override
                                                                public void onCancelled(DatabaseError databaseError) {

                                                                }
                                                            });


                      /*      mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if(task.isSuccessful()){
                                        // mRegProgress.dismiss();
                                        Intent mainIntent= new Intent(PhoneAuthActivity.this,MainActivity.class);
                                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(mainIntent);
                                        finish();
                                    }

                                }
                            });  */

                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                mVerificationField.setError("Invalid code.");
                            }
                        }
                    }
                });
    }


    private void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        mResendButton.setVisibility(View.VISIBLE);
        mVerifyButton.setVisibility(View.VISIBLE);

    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    private boolean validatePhoneNumber() {
        String phoneNumber = mPhoneNumberField.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberField.setError("Invalid phone number.");
            return false;
        }
        return true;
    }
    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            startActivity(new Intent(PhoneAuthActivity.this, MainActivity.class));
            finish();
        }
        if(!isNetworkAvailable()){

            mStartButton.setEnabled(false);
            mStartButton.setText("No Network Available!");
            mVerifyButton.setEnabled(false);
            mResendButton.setEnabled(false);

        }
//------------------NEW CODE FOR NETWORK MANAGEMENT-------------------------
       final Handler handler = new Handler();

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    while(true) {
                        sleep(500);
                        handler.post(this);
                        if(isNetworkAvailable()){

                            mStartButton.setEnabled(false);
                            mStartButton.setText("No Network Available");


                        } else{

                            mStartButton.setEnabled(true);
                            mStartButton.setText("Send OTP");

                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

     //   thread.start();
//----------------NEW CODE ENDS-----------------------------------------------

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_start_verification:
                Toast.makeText(PhoneAuthActivity.this,"Login Blocked!",Toast.LENGTH_SHORT).show();
           /*     if (!validatePhoneNumber()) {
                    return;
                }
                mRegProgress.setTitle("Registering User");
                mRegProgress.setMessage("Please wait while we send you the code via sms");
                mRegProgress.setCanceledOnTouchOutside(false);
                mRegProgress.show();
                startPhoneNumberVerification(mPhoneNumberField.getText().toString()); */
                break;
            case R.id.button_verify_phone:
                String code = mVerificationField.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    mVerificationField.setError("Cannot be empty.");
                    return;
                }

                verifyPhoneNumberWithCode(mVerificationId, code);
                break;
            case R.id.button_resend:
                resendVerificationCode(mPhoneNumberField.getText().toString(), mResendToken);
                break;
        }

    }

    public void showNameDialog(final String uid) {
        mRegProgress.dismiss();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_name_dialog, null);
        dialogBuilder.setView(dialogView);

        final CircleImageView uImage = (CircleImageView) dialogView.findViewById(R.id.custom_id);
        final EditText edt = (EditText) dialogView.findViewById(R.id.custom_name);
        final EditText edt2 = (EditText) dialogView.findViewById(R.id.custom_email);
        final SeekBar seekBar = (SeekBar) dialogView.findViewById(R.id.progressSeekBar);

        seekBar.animate();

        uImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1,1)
                        .start(PhoneAuthActivity.this);
            }
        });

        dialogBuilder.setTitle("Welcome User");
        dialogBuilder.setMessage("Please enter your name and email id to continue");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
               final String Name = edt.getText().toString();
               final String email = edt2.getText().toString();

               if(TextUtils.isEmpty(Name)){
                   edt.setError("Name cannot be empty");
               }
               if(TextUtils.isEmpty(email)){
                   edt2.setError("Email cannot be emoty");
               }
               if(!TextUtils.isEmpty(Name)||!TextUtils.isEmpty(email)) {
                   HashMap<String, String> userMap = new HashMap<String, String>();


                   final String device_token = FirebaseInstanceId.getInstance().getToken();

                   userMap.put("name", Name);
                   userMap.put("phone", mPhoneNumberField.getText().toString());
                   userMap.put("email", email);
                   userMap.put("device_token", device_token);
                   userMap.put("verify","notdone");
                   userMap.put("verificationImageUrl","default");
                   mDatabase.child(uid).setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                       @Override
                       public void onComplete(@NonNull Task<Void> task) {

                           if (task.isSuccessful()) {

                               Intent mainIntent= new Intent(PhoneAuthActivity.this,MainActivity.class);
                               mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                               startActivity(mainIntent);
                               finish();

                         }

                       }
                   });

               }
            }
        });

        AlertDialog b = dialogBuilder.create();
        b.show();
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                File thumb_filePath = new File(resultUri.getPath());
                //  FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();

                // String uid = current_user.getUid();

                //----------IMAGE COMPRESSION---------------

                Bitmap compressedImage = null;

                try {

                    compressedImage = new Compressor(this)
                            .setQuality(50)
                            .setMaxHeight(200)
                            .setMaxWidth(200)
                            .compressToBitmap(thumb_filePath);
                    Log.d("ImageCompression", "Success");

                } catch (java.io.IOException e) {
                    Log.d("ImageCompression", "Failure");
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                compressedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                final byte[] image_byte = baos.toByteArray();
                //imageByte = image_byte;
               // Bitmap bmp = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
                //userImage.setImageBitmap(Bitmap.createScaledBitmap(bmp, userImage.getWidth(),
                  //      userImage.getHeight(), false));
                //----------IMAGE COMPRESSION---------------


            }
        }
    }


}