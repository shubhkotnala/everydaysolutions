package com.dit.ananya.atservices;

import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.dd.CircularProgressButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgetPassword extends AppCompatActivity {

    private CircularProgressButton btnSubmit;
    private TextInputLayout txtEmail;
    private TextView forgetText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        getSupportActionBar().setElevation(0);

        txtEmail = (TextInputLayout) findViewById(R.id.forgetPasswordEmail);
        btnSubmit = (CircularProgressButton) findViewById(R.id.btnForgetPasswordSubmit);
        forgetText = (TextView) findViewById(R.id.txtForgetDesc);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = txtEmail.getEditText().getText().toString();

                FirebaseAuth.getInstance().sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if(task.isSuccessful()){

                            forgetText.setText("Instructions to reset your password are sent to your mail, please follow the instructions to reset your password.");

                        }else {

                            Toast.makeText(ForgetPassword.this,"Some error occured, Please try again later",Toast.LENGTH_SHORT).show();

                        }

                    }
                });

            }
        });

    }
}
