package com.dit.ananya.atservices;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.dd.CircularProgressButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class Register extends AppCompatActivity {

    private TextInputLayout mDisplayName;
    private TextInputLayout mPhone;
    private TextInputLayout mEmail;
    private TextInputLayout mPassword;
    private FirebaseAuth mAuth;
    private ProgressDialog mRegProgress;
    private ProgressDialog progressDialog;
    private StorageReference mImageStorage;
    private DatabaseReference mDatabase;
    private String dUrl = null;
    private CircleImageView userImage;

    private CircularProgressButton mUserImageBtn;
    private static final int GALLERY_PICK = 1;
    private CircularProgressButton mCreateBtn;
    byte[] imageByte = null;

    public Register() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);


        mAuth = FirebaseAuth.getInstance();
        getSupportActionBar().setElevation(0);
        mRegProgress = new ProgressDialog(this);

        mEmail = (TextInputLayout) findViewById(R.id.reg_email);
        mPassword = (TextInputLayout) findViewById(R.id.reg_password);
        mDisplayName=(TextInputLayout) findViewById(R.id.reg_display_name);

        mUserImageBtn = (CircularProgressButton) findViewById(R.id.btnImage);
        mCreateBtn = (CircularProgressButton) findViewById(R.id.reg_create_btn);
        userImage = (CircleImageView) findViewById(R.id.imgReg);
        mPhone=(TextInputLayout) findViewById(R.id.Phone);

        mCreateBtn.setIndeterminateProgressMode(true);

        mImageStorage = FirebaseStorage.getInstance().getReference();

        mUserImageBtn.setIndeterminateProgressMode(true);

        mUserImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1,1)
                        .start(Register.this);
            }
        });

        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1,1)
                        .start(Register.this);




            }
        });

        mCreateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCreateBtn.setProgress(50);
                String display_name = mDisplayName.getEditText().getText().toString();
                String email = mEmail.getEditText().getText().toString();
                String password = mPassword.getEditText().getText().toString();
                String phone = mPhone.getEditText().getText().toString();

                if (!TextUtils.isEmpty(display_name) || !TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)|| !TextUtils.isEmpty(phone)) {

                    mRegProgress.setTitle("Registering User");
                    mRegProgress.setMessage("Please wait while we create your account");
                    mRegProgress.setCanceledOnTouchOutside(false);
                    //mRegProgress.show();
                    register_user(display_name, email, password,phone);


                } else{
                    mCreateBtn.setProgress(-1);
                }

            }
        });
    }

    private void register_user(final String display_name, final String email, final String password,final String phone) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    mCreateBtn.setProgress(100);
                    final FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
                    final String uid = current_user.getUid();

                    current_user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if(task.isSuccessful()){

                                Toast.makeText(Register.this,"Verification email sent",Toast.LENGTH_SHORT).show();
                                //Image



                                if(imageByte != null) {
                                    StorageReference filepath = mImageStorage.child("user_images").child(uid + ".jpg");
                                    //progress dialog
                                    progressDialog = new ProgressDialog(Register.this);
                                    progressDialog.setMax(100);
                                    progressDialog.setMessage("Uploading...");
                                    progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                                     progressDialog.show();
                                    progressDialog.setCancelable(false);
                                    UploadTask uploadTask = filepath.putBytes(imageByte);
                                    // UploadTask uploadTask = filepath.putBytes(image_byte);
                                    uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                            //sets and increments value of progressbar
                                             progressDialog.incrementProgressBy((int) progress);
                                            mUserImageBtn.setProgress((int) progress);
                                        }
                                    }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                                            if (task.isSuccessful()) {

                                                progressDialog.dismiss();

                                                String download_url = task.getResult().getDownloadUrl().toString();

                                                if (download_url == null) {
                                                    download_url = "default";
                                                }

                                                //    dUrl = down.toString();
                                                mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);


                                                HashMap<String, String> userMap = new HashMap<String, String>();

                                                userMap.put("name", display_name);
                                                userMap.put("email", email);
                                                userMap.put("image", download_url);
                                                userMap.put("phone", phone);

                                                mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {

                                                        if (task.isSuccessful()) {
                                                            // mRegProgress.dismiss();
                                                            // progressDialog.dismiss();
                                                            mCreateBtn.setProgress(100);
                                                           /* Intent intent = new Intent(Register.this, MainActivity.class);
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                            startActivity(intent);
                                                            finish();  */
                                                        }

                                                    }
                                                });

                                            }

                                        }
                                    });
                                    //Image
                                } else {
                                    mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);


                                    HashMap<String, String> userMap = new HashMap<String, String>();

                                    userMap.put("name", display_name);
                                    userMap.put("email", email);
                                    userMap.put("image", "default");
                                    userMap.put("phone", phone);

                                    mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if (task.isSuccessful()) {
                                                // mRegProgress.dismiss();
                                                mCreateBtn.setProgress(100);
                                                // progressDialog.dismiss();
                                              /*  Intent intent = new Intent(Register.this, MainActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                                finish();  */
                                            }

                                        }
                                    });

                                }



                            }
                            else {
                                mRegProgress.hide();
                                Toast.makeText(Register.this,"Error sending verification email.\nTry again later",Toast.LENGTH_SHORT).show();

                            }

                        }
                    });


                } else {
                    mRegProgress.hide();
                    Toast.makeText(Register.this, "Cannot create your account. Please try again later.", Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            CropImage.activity(imageUri).setAspectRatio(1,1).start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                File thumb_filePath = new File(resultUri.getPath());
                //  FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();

                // String uid = current_user.getUid();

                //----------IMAGE COMPRESSION---------------

                Bitmap compressedImage = null;

                try {

                    compressedImage = new Compressor(this)
                            .setQuality(50)
                            .setMaxHeight(200)
                            .setMaxWidth(200)
                            .compressToBitmap(thumb_filePath);
                    Log.d("ImageCompression", "Success");

                } catch (java.io.IOException e) {
                    Log.d("ImageCompression", "Failure");
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                compressedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                final byte[] image_byte = baos.toByteArray();
                imageByte = image_byte;
                Bitmap bmp = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
                userImage.setImageBitmap(Bitmap.createScaledBitmap(bmp, userImage.getWidth(),
                        userImage.getHeight(), false));
                //----------IMAGE COMPRESSION---------------


            }
        }
    }
}