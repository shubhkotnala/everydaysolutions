package com.dit.ananya.atservices;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class AddWorkerActivity extends AppCompatActivity {

    private TextView fromTime,toTime;
    private CircleImageView profileImage;
    private AutoCompleteTextView cities;

    byte[] imageByte = null;

    final static private int GALLERY_PICK = 1;
    ArrayList<String> listAll=new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_worker);

        getSupportActionBar().setElevation(0);

        fromTime = (TextView) findViewById(R.id.addWorkFromTime);
        toTime = (TextView) findViewById(R.id.addWorkToTime);
        cities = (AutoCompleteTextView) findViewById(R.id.addWorkCity);

        setupTimePicker();
        setupUserData();
        setupCities();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,listAll);
        cities.setAdapter(adapter);

    }

    public void setupTimePicker(){


        fromTime.setOnClickListener(view ->
        {
            Calendar currentTime = Calendar.getInstance();
            int currentHour = currentTime.get(Calendar.HOUR_OF_DAY);
            int currentMin = currentTime.get(Calendar.MINUTE);
            TimePickerDialog timePickerDialog;
            timePickerDialog = new TimePickerDialog(AddWorkerActivity.this, (timePicker,hour,min)->{
                fromTime.setText(hour + " : " + min);
            },currentHour,currentMin,true); //true for 24Hours format

            timePickerDialog.setTitle("Start Time");
            timePickerDialog.show();

        });

        toTime.setOnClickListener(view -> {
            Calendar currentTime = Calendar.getInstance();
            int currentHour = currentTime.get(Calendar.HOUR_OF_DAY);
            int currentMin = currentTime.get(Calendar.MINUTE);
            TimePickerDialog timePickerDialog;
            timePickerDialog = new TimePickerDialog(AddWorkerActivity.this,(timePicker,hour,min) -> {
                toTime.setText(hour + " : " + min);
            },currentHour,currentMin,true);

            timePickerDialog.setTitle("End Time");
            timePickerDialog.show();

        });

    }

    public void setupUserData(){

        profileImage = (CircleImageView) findViewById(R.id.addWorkImage);
        profileImage.setOnClickListener(view -> {

            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(AddWorkerActivity.this);

        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            CropImage.activity(imageUri).setAspectRatio(1, 1).start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                File thumb_filePath = new File(resultUri.getPath());
                //  FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();

                // String uid = current_user.getUid();

                //----------IMAGE COMPRESSION---------------

                Bitmap compressedImage = null;

                try {

                    compressedImage = new Compressor(this)
                            .setQuality(50)
                            .setMaxHeight(200)
                            .setMaxWidth(200)
                            .compressToBitmap(thumb_filePath);
                    Log.d("ImageCompression", "Success");

                } catch (java.io.IOException e) {
                    Log.d("ImageCompression", "Failure");
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                compressedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                final byte[] image_byte = baos.toByteArray();
                imageByte = image_byte;
                Bitmap bmp = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
                profileImage.setImageBitmap(Bitmap.createScaledBitmap(bmp, profileImage.getWidth(),
                        profileImage.getHeight(), false));
                //----------IMAGE COMPRESSION---------------


            }
        }
    }

    // This add all JSON object's data to the respective lists
    void setupCities()
    {
        // Exceptions are returned by JSONObject when the object cannot be created
        try
        {
            // Convert the string returned to a JSON object
            JSONObject jsonObject=new JSONObject(getJson());
            // Get Json array
            JSONArray array=jsonObject.getJSONArray("array");
            // Navigate through an array item one by one
            for(int i=0;i<array.length();i++)
            {
                // select the particular JSON data
                JSONObject object=array.getJSONObject(i);
                String city=object.getString("name");
                String state=object.getString("state");
                // add to the lists in the specified format

                listAll.add(city+" , "+state);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
    public String getJson()
    {
        String json=null;
        try
        {
            // Opening cities.json file
            InputStream is = this.getAssets().open("cities.json");
            // InputStream is = this.getResources().openRawResource(R.raw.cities);
            // is there any content in the file
            int size = is.available();
            byte[] buffer = new byte[size];
            // read values in the byte array
            is.read(buffer);
            // close the stream --- very important
            is.close();
            // convert byte to string
            json = new String(buffer, "UTF-8");
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            Log.d("JSON","IO Exception");
            return json;
        }
        return json;
    }

}