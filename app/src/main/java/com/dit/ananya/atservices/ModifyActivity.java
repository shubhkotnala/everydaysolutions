package com.dit.ananya.atservices;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;

public class ModifyActivity extends AppCompatActivity {

    private EditText userDate,userVehicle,userTime,userfrom,userto,userseat,usercname;
    private int SELECTED = 6;
    String place,to,from,phone,vc,cn,seat,date,time,email,name,uid,image,verify,addre;
    private final int REQUEST_CODE_PLACEPICKER = 1;
    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    Button saveBtn;
    private DatabaseReference mLocationDatabase,mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify);

        userDate = (EditText) findViewById(R.id.userDefaultDate);
        userVehicle = (EditText) findViewById(R.id.uservname);
        userTime= (EditText) findViewById(R.id.userDefaultTime);
        userfrom= (EditText) findViewById(R.id.from);
        userto= (EditText) findViewById(R.id.to);
        userseat= (EditText) findViewById(R.id.seats);
        usercname=(EditText)findViewById(R.id.usercname);
        saveBtn=(Button)findViewById(R.id.saveBtn);

        FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
        final String uid = current_user.getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Location").child(uid);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                from = dataSnapshot.child("from").getValue().toString();
                to = dataSnapshot.child("to").getValue().toString();
                date = dataSnapshot.child("date").getValue().toString();
                time = dataSnapshot.child("time").getValue().toString();
                seat = dataSnapshot.child("capacity").getValue().toString();
                vc = dataSnapshot.child("vehicle").getValue().toString();
                cn = dataSnapshot.child("number").getValue().toString();
                email = dataSnapshot.child("email").getValue().toString();
                name = dataSnapshot.child("name").getValue().toString();
                image = dataSnapshot.child("image").getValue().toString();
                verify = dataSnapshot.child("verify").getValue().toString();
                addre=dataSnapshot.child("address").getValue().toString();
                phone=dataSnapshot.child("phone").getValue().toString();




                userfrom.setText(from);
                userto.setText(to);
                userDate.setText(date);
                userTime.setText(time);
                userseat.setText(seat);
                userVehicle.setText(vc);
                usercname.setText(cn);






            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });







        userfrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //    startPlacePickerActivity();

                SELECTED = 3;

                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                    .build(ModifyActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

                    //   spf.setText("" + from);

                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
                // from= place;

            }
        });
        userto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SELECTED = 2;

                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                    .build(ModifyActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);



                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
                // to = place;
            }

        });

        userDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker;
                mDatePicker = new DatePickerDialog(ModifyActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                    /*      Your code   to get date and time    */
                        selectedmonth = selectedmonth + 1;
                        userDate.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select Date");
                mDatePicker.show();
            }
        });

        userTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(ModifyActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        userTime.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

         vc = userVehicle.getText().toString().trim();
         cn = usercname.getText().toString().trim();
         seat = userseat.getText().toString().trim();



        mLocationDatabase=FirebaseDatabase.getInstance().getReference().child("Location").child(uid);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                if (TextUtils.isEmpty(vc) || TextUtils.isEmpty(cn) || TextUtils.isEmpty(seat)) {

                    if(TextUtils.isEmpty(cn)){
                        usercname.setError("Enter Vehicle number");
                    }
                    if(TextUtils.isEmpty(vc)){
                        userVehicle.setError("Enter Vehicle Name");
                    }
                    if(TextUtils.isEmpty(seat)){
                        userseat.setError("Enter Vehicle capacity");
                    } if (userTime == null){
                        userTime.setError("Enter time");
                    }if (userDate == null){
                        userDate.setError("Enter Date");
                    }

                } else if(from.equals("SELECT LOCATION")||to.equals("SELECT LOCATION")){
                    // Toast.makeText(havevehicle.this,"Select Location",Toast.LENGTH_SHORT).show();
                } else{

                    String time = userTime.getText().toString();
                    final String date = userDate.getText().toString();

                    //vc.equals("")||cn.equals(""))

                    HashMap<String,String> userMap= new HashMap<String, String>();

                    userMap.put("phone",phone);
                    userMap.put("verify",verify);
                    userMap.put("name",name);
                    userMap.put("email",email);
                    userMap.put("image",image);
                    userMap.put("from",from);
                    userMap.put("to",to);
                    userMap.put("address",addre);
                    //userMap.put("vehicle",vc);
                    //userMap.put("number",cn);
                    userMap.put("capacity",seat);
                    userMap.put("time",time);
                    userMap.put("date",date);
                    userMap.put("uid",uid);
                    userMap.put("search",from+to+date+"hello");
                    mLocationDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if(task.isSuccessful()){
                               Intent intent = new Intent(ModifyActivity.this,MainActivity.class);

                                intent.putExtra("fromv",from);
                                intent.putExtra("tov",to);
                                intent.putExtra("date",date);

                                startActivity(intent);
                                //   Toast.makeText(havevehicle.this,"Successful", Toast.LENGTH_LONG).show();
                            }
                        }

                    });

                 /*   else {

                    HashMap<String,String> userMap= new HashMap<String, String>();

                    userMap.put("phone",phone);
                    userMap.put("verify",verify);
                    userMap.put("name",name);
                    userMap.put("email",email);
                    userMap.put("image",image);
                    userMap.put("from",from);
                    userMap.put("to",to);
                    userMap.put("address",addre);
                    userMap.put("vehicle",vc);
                    userMap.put("number",cn);
                    userMap.put("capacity",seat);
                    userMap.put("time",time);
                    userMap.put("date",date);
                    userMap.put("uid",uid);
                    userMap.put("search",from+to+date+"hello");
                    mLocationDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if(task.isSuccessful()){
                               Intent intent = new Intent(ModifyActivity.this,MainActivity.class);

                                intent.putExtra("fromv",from);
                                intent.putExtra("tov",to);
                                intent.putExtra("date",date);

                                startActivity(intent);
                                //   Toast.makeText(havevehicle.this,"Successful", Toast.LENGTH_LONG).show();
                            }
                        }

                    });    } */

                }

            }
        });













    }












    private void startPlacePickerActivity() {


        PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
        // this would only work if you  your Google Places API working

        try {
            Intent intent = intentBuilder.build(this);
            startActivityForResult(intent, REQUEST_CODE_PLACEPICKER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void displaySelectedPlaceFromPlacePicker(Intent data) {
        place = "";
        Place placeSelected = PlacePicker.getPlace(data, this);
        place = placeSelected.getName().toString();
        String address=placeSelected.getAddress().toString();
        if(SELECTED == 2){

            userto.setText(place);
            to=place;

        } else if(SELECTED == 3){
            userfrom.setText(place);
            from=place;
             addre=address;

        }

    }
    protected  void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PLACEPICKER && resultCode == RESULT_OK) {

            displaySelectedPlaceFromPlacePicker(data);

        }

        if(requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE && resultCode == RESULT_OK){

            displaySelectedPlaceFromPlacePicker(data);

        }
    }



}



