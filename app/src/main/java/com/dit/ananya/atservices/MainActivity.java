package com.dit.ananya.atservices;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.robertlevonyan.views.customfloatingactionbutton.FloatingActionLayout;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private Button bt1;
    private Button bt2;
    private Button btnLogout;
    private TextView txtVerify;
    private String uid=null;
    private DatabaseReference mDatabase;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FirebaseApp.initializeApp(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FloatingActionLayout verify = (FloatingActionLayout) findViewById(R.id.verify);
        verify.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,VerificationActivity.class));

            }
        });

         FloatingActionLayout logout = (FloatingActionLayout) findViewById(R.id.logout);
        logout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Toast.makeText(MainActivity.this,"Logged Out",Toast.LENGTH_SHORT).show();
                Intent mainIntent= new Intent(MainActivity.this,StartActivity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(mainIntent);
                finish();

            }
        });

         FloatingActionLayout modify = (FloatingActionLayout) findViewById(R.id.modify);
        modify.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,ModifyActivity.class));

            }
        });
         FloatingActionLayout profile = (FloatingActionLayout) findViewById(R.id.profile);
        profile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,SettingsActivity.class));

            }
        });

        if(FirebaseAuth.getInstance().getCurrentUser()!=null) {


            FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
            uid = current_user.getUid();

            mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

            mAuth = FirebaseAuth.getInstance();
            bt1 = (Button) findViewById(R.id.bt1);
            bt2 = (Button) findViewById(R.id.bt2);

            txtVerify = (TextView) findViewById(R.id.txtVerify);

            getSupportActionBar().setElevation(0);
            FirebaseUser currentUser = mAuth.getCurrentUser();
            // startActivity(new Intent(MainActivity.this,novehicle.class));

            txtVerify.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, VerificationActivity.class));
                }
            });

            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String verify = dataSnapshot.child("verify").getValue().toString();

                    if (verify.equals("notdone")) {

                        txtVerify.setVisibility(View.VISIBLE);
                        txtVerify.setText("Your are not yet a verified user.\nTo verify yourself click here.");

                    } else if (verify.equals("done")) {

                        txtVerify.setVisibility(View.GONE);

                    } else if (verify.equals("discarded")) {

                        txtVerify.setVisibility(View.VISIBLE);
                        txtVerify.setText("Your request for ID verification is REJECTED, please upload your ID again to verify yourself");

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            if (currentUser == null) {
                startActivity(new Intent(MainActivity.this, StartActivity.class));
            }

            if (mAuth.getCurrentUser() != null) {
                DatabaseReference mUserRef = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getCurrentUser().getUid());


                bt1.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, havevehicle.class);
                        startActivity(intent);
                    }

                });


                bt2.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, novehicle.class);
                        startActivity(intent);
                    }

                });
            }
        } else{
            startActivity(new Intent(MainActivity.this,StartActivity.class));
            finish();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();

        if (FirebaseAuth.getInstance().getCurrentUser() == null) {

            startActivity(new Intent(MainActivity.this,StartActivity.class));
            finish();

        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.menu_main_setting){
            startActivity(new Intent(MainActivity.this,SettingsActivity.class));
        }
        if (item.getItemId() == R.id.menu_main_logout){
            FirebaseAuth.getInstance().signOut();
            Toast.makeText(MainActivity.this,"Logged Out",Toast.LENGTH_SHORT).show();
            Intent mainIntent= new Intent(MainActivity.this,StartActivity.class);
            mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(mainIntent);
            finish();
        }
        if (item.getItemId() == R.id.menu_main_id){
            startActivity(new Intent(MainActivity.this,VerificationActivity.class));
        }
        if (item.getItemId() == R.id.menu_main_modify){
            startActivity(new Intent(MainActivity.this,ModifyActivity.class));
        }

        return true;

    }
}