package com.dit.ananya.atservices;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.dd.CircularProgressButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class SettingsActivity extends AppCompatActivity {

    private CircleImageView userImage;
    private ProgressDialog mRegProgress;
    private ProgressDialog progressDialog;
    private StorageReference mImageStorage;
    private DatabaseReference mDatabase;
    private String dUrl = null;
    private static final int GALLERY_PICK = 1;
    private CircularProgressButton mCreateBtn;
    private String uid = null;
    private Button mUserImageBtn,mSaveBtn;
    private String mUserPhone="Not Available",mUserEmail="Not Available",mUSerName="Default Name";

    private TextInputLayout email = null;

    private TextView phone,name;


    byte[] imageByte = null;
    String image = "default";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mSaveBtn = (Button) findViewById(R.id.btnSettingsSave);
        userImage = (CircleImageView) findViewById(R.id.settingUserImage);
        mImageStorage = FirebaseStorage.getInstance().getReference();


        email = (TextInputLayout) findViewById(R.id.settingsEmail);
        FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();

         uid = current_user.getUid();

        phone = (TextView) findViewById(R.id.phoneno);
        name = (TextView) findViewById(R.id.settingsName);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);


        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                image = dataSnapshot.child("verificationImageUrl").getValue().toString();
                mUserPhone = dataSnapshot.child("phone").getValue().toString();
                mUserEmail = dataSnapshot.child("email").getValue().toString();
                mUSerName = dataSnapshot.child("name").getValue().toString();

                Picasso.with(SettingsActivity.this).load(image).into(userImage);
                phone.setText(mUserPhone);
                email.getEditText().setText(mUserEmail);
                name.setText(mUSerName);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                verifyuser();

            }
        });

        Picasso.with(this).load(image).placeholder(R.drawable.defaultpic).into(userImage);

        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1,1)
                        .start(SettingsActivity.this);

            }
        });

    }
  //  TextView phoneno= (TextView) findViewById(R.id.phoneno);
//    TextInputLayout emailid= (TextInputLayout)findViewById(R.id.settingsEmail);



    private void verifyuser() {
        if (imageByte != null) {
            StorageReference filepath = mImageStorage.child("user_images").child(uid + ".jpg");
            //progress dialog
           final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMax(100);
            progressDialog.setMessage("Uploading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.show();
            progressDialog.setCancelable(false);
            UploadTask uploadTask = filepath.putBytes(imageByte);
            // UploadTask uploadTask = filepath.putBytes(image_byte);
            uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    //sets and increments value of progressbar
                    progressDialog.incrementProgressBy((int) progress);
                }
            }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                    if (task.isSuccessful()) {

                        progressDialog.dismiss();

                        String download_url = task.getResult().getDownloadUrl().toString();

                        if (download_url == null) {
                            download_url = "default";
                        }

                        //    dUrl = down.toString();

                        final String emailText = email.getEditText().getText().toString();

                        if(!emailText.isEmpty() && Patterns.EMAIL_ADDRESS.matcher(emailText).matches()){
                            mDatabase.child("email").setValue(emailText);
                        }
                        mDatabase.child("verificationImageUrl").setValue(download_url);

                        Toast.makeText(SettingsActivity.this,"You will be verified once we check your details. Check your account details to check if you are verified or not",Toast.LENGTH_LONG);
                        Intent mainIntent= new Intent(SettingsActivity.this,MainActivity.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(mainIntent);
                        finish();

                    }

                }


            });


        } else {
            // mRegProgress.hide();
            Toast.makeText(SettingsActivity.this, "Please select an image to continue.", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            CropImage.activity(imageUri).setAspectRatio(1,1).start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                File thumb_filePath = new File(resultUri.getPath());

                //----------IMAGE COMPRESSION---------------

                Bitmap compressedImage = null;

                try {

                    compressedImage = new Compressor(this)
                            .setQuality(50)
                            .setMaxHeight(200)
                            .setMaxWidth(200)
                            .compressToBitmap(thumb_filePath);
                    Log.d("ImageCompression", "Success");

                } catch (java.io.IOException e) {
                    Log.d("ImageCompression", "Failure");
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                compressedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                final byte[] image_byte = baos.toByteArray();
                imageByte = image_byte;
                Bitmap bmp = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
                userImage.setImageBitmap(Bitmap.createScaledBitmap(bmp, userImage.getWidth(),
                        userImage.getHeight(), false));
                //----------IMAGE COMPRESSION---------------

//                verifyuser();

            }
        }
    }

}
