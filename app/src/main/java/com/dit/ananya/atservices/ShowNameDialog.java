package com.dit.ananya.atservices;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dd.CircularProgressButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class ShowNameDialog extends AppCompatActivity {


    private ProgressDialog mRegProgress;
    private ProgressDialog progressDialog;
    private StorageReference mImageStorage;
    private DatabaseReference mDatabase;
    private static final int GALLERY_PICK = 1;
    byte[] imageByte = null;

    private CircleImageView uImage;
    private EditText uName,uEmail;
    private Button btnDone,btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_name_dialog);

        getSupportActionBar().setElevation(0);
      //  mRegProgress = new ProgressDialog(this);

        uImage = (CircleImageView) findViewById(R.id.custom_id);
        uName = (EditText) findViewById(R.id.custom_name);
        uEmail =(EditText) findViewById(R.id.custom_email);

        btnDone = (Button) findViewById(R.id.btnShowNameDone);
        btnCancel = (Button) findViewById(R.id.btnShowNameCancel);

        final String phone = getIntent().getStringExtra("phone");
        final String uid = getIntent().getStringExtra("uid");

        mDatabase= FirebaseDatabase.getInstance().getReference().child("Users");
        mImageStorage = FirebaseStorage.getInstance().getReference();

        //btnDone.setIndeterminateProgressMode(true);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String name = uName.getText().toString();
                final String email = uEmail.getText().toString();

                if(TextUtils.isEmpty(name)){
                    uName.setError("Enter name");
                } if (TextUtils.isEmpty(email)){

                    if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                        uEmail.setError("Enter valid email");
                    }else{
                        uEmail.setError("Enter Email");
                    }

                }
                if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()){

                    final String device_token = FirebaseInstanceId.getInstance().getToken();



                    if(imageByte != null) {
                        StorageReference filepath = mImageStorage.child("user_images").child(uid + ".jpg");
                        //progress dialog
                        progressDialog = new ProgressDialog(ShowNameDialog.this);
                        progressDialog.setMax(100);
                        progressDialog.setMessage("Uploading...");
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                        progressDialog.show();
                        progressDialog.setCancelable(false);
                        UploadTask uploadTask = filepath.putBytes(imageByte);
                        // UploadTask uploadTask = filepath.putBytes(image_byte);
                        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                //sets and increments value of progressbar
                                progressDialog.incrementProgressBy((int) progress);
                                //btnDone.setProgress((int) progress);
                            }
                        }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                                if (task.isSuccessful()) {

                                    progressDialog.dismiss();

                                    String download_url = task.getResult().getDownloadUrl().toString();

                                    if (download_url == null) {
                                        download_url = "default";
                                    }

                                    //    dUrl = down.toString();
                                    mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);


                                    HashMap<String, String> userMap = new HashMap<String, String>();

                                    userMap.put("name", name);
                                    userMap.put("email", email);
                                    userMap.put("image", download_url);
                                    userMap.put("phone", phone);
                                    userMap.put("device_token",device_token);
                                    userMap.put("verificationImageUrl","default");
                                    userMap.put("verify","notdone");


                                    mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if (task.isSuccessful()) {
                                                 //mRegProgress.dismiss();
                                                 progressDialog.dismiss();

                                                            Intent intent = new Intent(ShowNameDialog.this, MainActivity.class);
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                            startActivity(intent);
                                                            finish();
                                            }

                                        }
                                    });

                                }

                            }
                        });
                        //Image
                    }else {
                        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);


                        HashMap<String, String> userMap = new HashMap<String, String>();

                        userMap.put("name", name);
                        userMap.put("email", email);
                        userMap.put("image", "default");
                        userMap.put("phone", phone);
                        userMap.put("device_token",device_token);
                        userMap.put("verificationImageUrl","default");
                        userMap.put("verify","notdone");


                        mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                if (task.isSuccessful()) {
//                                     mRegProgress.dismiss();

                                     progressDialog.dismiss();
                                                Intent intent = new Intent(ShowNameDialog.this, MainActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                                finish();
                                }

                            }
                        });

                    }



                }

                }


        });

        uImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1,1)
                        .start(ShowNameDialog.this);

            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            CropImage.activity(imageUri).setAspectRatio(1,1).start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                File thumb_filePath = new File(resultUri.getPath());
                //  FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();

                // String uid = current_user.getUid();

                //----------IMAGE COMPRESSION---------------

                Bitmap compressedImage = null;

                try {

                    compressedImage = new Compressor(this)
                            .setQuality(50)
                            .setMaxHeight(200)
                            .setMaxWidth(200)
                            .compressToBitmap(thumb_filePath);
                    Log.d("ImageCompression", "Success");

                } catch (java.io.IOException e) {
                    Log.d("ImageCompression", "Failure");
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                compressedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                final byte[] image_byte = baos.toByteArray();
                imageByte = image_byte;
                Bitmap bmp = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
                uImage.setImageBitmap(Bitmap.createScaledBitmap(bmp, uImage.getWidth(),
                        uImage.getHeight(), false));
                //----------IMAGE COMPRESSION---------------


            }
        }
    }

}
