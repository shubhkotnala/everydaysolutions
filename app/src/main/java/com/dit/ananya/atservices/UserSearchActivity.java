package com.dit.ananya.atservices;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserSearchActivity extends AppCompatActivity {

    private CircleImageView userImage;
    private TextView userName,userDate,userVehicle,userPhone,userTime,userVerify,useraddress,userseat;
    private String verify;
    private String uid;
    private String from;
    private String to;
    private String cUid;
    private String address;
    private DatabaseReference mDatabase;
    private ImageView verifyimage;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_search);

        getSupportActionBar().setElevation(0);



        userName = (TextView) findViewById(R.id.userDefaultName);
        userDate = (TextView) findViewById(R.id.userDefaultDate);
        userVehicle = (TextView) findViewById(R.id.userDefaultVehicle);
        userPhone = (TextView) findViewById(R.id.userDefaultPhone);
        userImage = (CircleImageView) findViewById(R.id.userDefaultImage);
        userTime= (TextView) findViewById(R.id.userDefaultTime);
        userVerify= (TextView) findViewById(R.id.verify);
        verifyimage=(ImageView)findViewById(R.id.imageverify);
        useraddress=(TextView)findViewById(R.id.address);
        userseat=(TextView)findViewById(R.id.seats);
        Bundle extras = getIntent().getExtras();
        userName.setText(extras.getString("name"));
        userVehicle.setText(extras.getString("vehicle"));
        userPhone.setText(extras.getString("phone"));
        userDate.setText(extras.getString("date"));
        userTime.setText(extras.getString("time"));
        useraddress.setText(extras.getString("address"));
        userseat.setText(extras.getString("capacity"));
        verify=extras.getString("verify");
        uid=extras.getString("uid");
        mDatabase = FirebaseDatabase.getInstance().getReference("Location");

        cUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        if(cUid.equals(uid)){



        }

        mDatabase.child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                from = dataSnapshot.child("from").getValue().toString();
                to = dataSnapshot.child("to").getValue().toString();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Picasso.with(this).load(extras.getString("image")).placeholder(R.drawable.defaultpic).into(userImage);
        if(Objects.equals(verify, "done"))
        {
            userVerify.setText("This ID is a Goverement verified User");
            verifyimage.setEnabled(true);

        }
        if(Objects.equals(verify, "notdone"))

        {
            userVerify.setText("This Id is not Goverement verified User");
            verifyimage.setEnabled(false);
        }
        if(Objects.equals(verify, "discarded"))

        {
            userVerify.setText("This Id is not Goverement verified User");
            verifyimage.setEnabled(false);
        }

    }
}
